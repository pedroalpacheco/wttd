from django.apps import AppConfig

class SubscriptionsConfig(AppConfig):
    name = 'eventex.subscriptions'
    verbose_name = 'Contorle de Participantes'