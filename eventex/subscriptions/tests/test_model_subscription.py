from datetime import datetime

from django.test import TestCase
from eventex.subscriptions.models import Subscription

class SubscriptionmodelTest(TestCase):
    def setUp(self):
        self.obj = Subscription(
            name='Pedro Pacheco',
            cpf='12345678910',
            email='pedro@gmail.com',
            phone='47-999949078',
        )
        self.obj.save()

    def test_creat(self):
        self.assertTrue(Subscription.objects.exists())

    def test_created_at(self):
        """Subscription must have an auto created_at attr"""
        self.assertIsInstance(self.obj.created_at, datetime)

    def test_str(self):
        self.assertEquals('Pedro Pacheco', str(self.obj ))

    def __str__(self):
        return self.name

    def test_paid_default_to_False(self):
        """By default paid must be False"""
        self.assertEqual(False, self.obj.paid)