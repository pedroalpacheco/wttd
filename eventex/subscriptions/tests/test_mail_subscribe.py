from django.core import mail
from django.test import TestCase
from django.shortcuts import resolve_url as r


class SubscribePostValid(TestCase):
    def setUp(self):
        data = dict(name='Pedro Pacheco', cpf='12345678901',
                    email='pedro@net.com', phone='47-9995-0623')
        self.client.post(r('subscriptions:new'), data)
        self.email = mail.outbox[0]

    def test_subscription_email_subject(self):
        expect = "Confirmação de inscrição"

        self.assertEqual(expect, self.email.subject)

    def test_subscription_email_from(self):

        expect = 'contato@eventex.com.br'

        self.assertEqual(expect, self.email.from_email)

    def test_subscription_email_to(self):

        expect = ['contato@eventex.com.br','pedro@net.com']

        self.assertEqual(expect, self.email.to)

    def test_subscription_email_body(self):
        contents = [
            'Pedro Pacheco',
            '12345678901',
            'pedro@net.com',
            '47-9995-0623',
        ]

        for content in contents:
            with self.subTest():
                self.assertIn(content, self.email.body)
